# General space class
class Space:

    # Init function which sets properties
    def __init__(self, name, message=None, routes=[], contains=[]):
        self.name = name
        self.routes = routes
        self.contains = contains
        self.message = message

    def __str__(self):
        return self.name

    # Look function that lists items in the room and the routes
    def look(self, world):

        if len(self.contains) != 0:

            if len(self.contains) > 1:
                text =  "There is a {} and a {} here.".format(
                        ", ".join(map(str, self.contains[0:-1])),
                self.contains[-1].name)

            else:
                text =  "There is a {} here.".format(str(self.contains[0]))

        else:
            text = "There is nothing here."

        if self.routes != []:

            if len(self.routes) == 1:
                text += "\nThere is a {} to the {}.".format(str(world[self.routes[0][0]]),
                self.routes[0][1])

            else:
                text +=  "\nThere is a {} and a {} to the {}.".format(
                ", ".join([str(world[x]) + " to the " + y for x, y in self.routes[0:-1]]),
                str(world[self.routes[-1][0]]), self.routes[-1][1])

        print (text)

    def addItems(self, items):
        self.contains += items

    def addItem(self, item):
        self.contains.append(item)

    def addRoute(self, id, dir):
        self.routes.append([id, dir])
