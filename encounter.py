import random

# Main encounter class
class Encounter:

    # Initializer
    def __init__(self, message, location=None, history=[], item=None, combat=None, chance=100):
        self.message = message
        self.location = location
        self.item = item
        self.history = history
        self.combat = combat
        self.chance = chance

    # Method that checks if the encounter can be had
    def check(self, location, history, bag):

        if self.item != None:
            got = False
            for i in bag:
                if self.item in i.name:
                    got = True
                    break
        else:
            got = True

        done = True
        for i in self.history:
            if i not in history:
                done = False
                break

        val = random.choice(range(100))
        if got and done and self.location in [None, location] and val <= self.chance:
            return True
        else:
            return False
