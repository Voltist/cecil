# General item class
class Item:

    # Init function that sets properties
    def __init__(self, name, movable, contains=[], prefix=None):
        self.name = name
        self.movable = movable
        self.contains = contains
        self.prefix = prefix

    # Str function call, which returns the name
    def __str__(self):
        return self.name

    # Look function
    def look(self):
        if self.contains not in [None, []]:

            if len(self.contains) > 1:
                text =  "{} the {}, there is a {} and a {}.".format(
                self.prefix.capitalize(), self.name, ", ".join(map(str, self.contains[0:-1])),
                str(self.contains[-1]))

            else:
                text =  "{} the {}, there is a {}.".format(
                self.prefix.capitalize(), self.name, str(self.contains[0]))

        else:
            text = "The {} is of no particular interest.".format(self.name)

        # Return text
        print (text)

# Weapon class
class Weapon(Item):

    # Init function that sets properties
    def __init__(self, name, sharpness=0, pointiness=0, bluntness=0, magic=False):
        self.name = name
        self.movable = True
        self.contains = None
        self.prefix = None
        self.sharpness = sharpness
        self.pointiness = pointiness
        self.bluntness = bluntness
        self.magic = magic

class Note(Item):

    # Init function that sets properties
    def __init__(self, name, text):
        self.name = name
        self.movable = True
        self.contains = None
        self.prefix = None
        self.text = text

    def read(self):
        print ("The {} says:\n{}\nEnd.".format(self.name, self.text))

class PlotItem(Item):

    # Init function that sets properties
    def __init__(self, name, text):
        self.name = name
        self.movable = True
        self.contains = None
        self.prefix = None
