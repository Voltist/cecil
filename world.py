from item import Item, Weapon, Note, PlotItem
from space import Space
from sub import title, combat
from character import Enemy
from encounter import Encounter
from copy import deepcopy as dc

world = {}
encounters = []

compass =          ["north", "south", "east", "west", "northwest", "northeast", "southwest", "southeast"]
compass_inverted = ["south", "north", "west", "east", "southeast", "southwest", "northeast", "northwest"]

def link(id1, dir, id2):
    global world
    world[id1].addRoute(id2, dir)
    world[id2].addRoute(id1, compass_inverted[compass.index(dir)])

# Generics
corpse = Item("corpse", False)
pile = Item("pile of desks", False)
papers = Item("loose papers", False)
crash = Item("large black rock in a small crater", False)
shrubs = Item("burnt shrubs", False)
bag = Item("school bag", True)
shelf = Item("bookshelf", False)
goo = Item("shiny black goo", False)

magic = Weapon("magic", 1, 1, 5, magic=True)
minion = Enemy("figure in a voidly-black cloak", 10, magic)


# English block

# Courtyard
world["english_courtyard"] = dc(Space("courtyard"))

# Corridor junctions
world["w_english_junction"] = dc(Space("corridor junction"))
world["e_english_junction"] = dc(Space("corridor junction"))

# Corridors
world["nw_english_corridor"] = dc(Space("corridor"))
world["ne_english_corridor"] = dc(Space("corridor"))
world["sw_english_corridor"] = dc(Space("corridor"))
world["se_english_corridor"] = dc(Space("corridor"))

# Classrooms
world["spronken_room"] = dc(Space("classroom"))
world["latin_room"] = dc(Space("classroom"))
world["robins_room"] = dc(Space("classroom"))
world["ashby_room"] = dc(Space("classroom"))

# Link junctions with courtyard
link("english_courtyard", "west", "w_english_junction")
link("english_courtyard", "east", "e_english_junction")

# Link corridors with junctions
link("w_english_junction", "north", "nw_english_corridor")
link("w_english_junction", "south", "sw_english_corridor")
link("e_english_junction", "north", "ne_english_corridor")
link("e_english_junction", "south", "se_english_corridor")

# Link classrooms with corridors
link("nw_english_corridor", "west", "spronken_room")
link("ne_english_corridor", "east", "latin_room")
link("sw_english_corridor", "east", "robins_room")
link("se_english_corridor", "west", "ashby_room")

# Add message to courtyard
world["english_courtyard"].message = "The sky is black and you cannot see the city."

# Add messages to corridors
world["nw_english_corridor"].message = "The lights are flickering here."
world["ne_english_corridor"].message = "The lights are flickering here."
world["sw_english_corridor"].message = "The lights are flickering here."
world["se_english_corridor"].message = "The lights are flickering here."

# Add messages to classrooms
world["spronken_room"].message = "It is dark here, and the walls are plastered with posters about drilling devices."
world["latin_room"].message = "There are Latin words on the walls here."
world["ashby_room"].message = "It is very cold here, and there are drawings pinned to the walls."

# Populate courtyard with items
world["english_courtyard"].addItem(shrubs)

# Populate junctions with items
world["e_english_junction"].addItem(corpse)

# Populate corridors with items
world["nw_english_corridor"].addItems([corpse, crash])
world["sw_english_corridor"].addItems([corpse, papers, pile])
world["ne_english_corridor"].addItems([pile, corpse])
world["se_english_corridor"].addItems([papers, crash])

# Populate classrooms with generic items
world["latin_room"].addItems([shelf, corpse, goo])
world["robins_room"].addItems([bag, shelf, corpse])
world["ashby_room"].addItems([bag, goo])


# Science block

# Courtyard
world["science_courtyard"] = dc(Space("courtyard"))

# Corridor junctions
world["w_science_junction"] = dc(Space("junction"))
world["e_science_junction"] = dc(Space("junction"))

# Corridors
world["nw_science_corridor"] = dc(Space("corridor"))
world["ne_science_corridor"] = dc(Space("corridor"))
world["sw_science_corridor"] = dc(Space("corridor"))
world["se_science_corridor"] = dc(Space("corridor"))
world["n_science_corridor"] = dc(Space("corridor"))

# Classrooms
world["prime_room"] = dc(Space("classroom"))
world["caufield_room"] = dc(Space("classroom"))
world["pirie_room"] = dc(Space("classroom"))

# Link courtyard with junctions
link("science_courtyard", "west", "w_science_junction")
link("science_courtyard", "east", "e_science_junction")

# Link junctions with corridors
link("w_science_junction", "north", "nw_science_corridor")
link("w_science_junction", "south", "sw_science_corridor")
link("e_science_junction", "north", "ne_science_corridor")
link("e_science_junction", "south", "se_science_corridor")

# Link corridors together
link("nw_science_corridor", "northeast", "n_science_corridor")
link("ne_science_corridor", "northwest", "n_science_corridor")

# Link corridors with classrooms
link("nw_science_corridor", "west", "prime_room")
link("n_science_corridor", "south", "caufield_room")
link("se_science_corridor", "west", "pirie_room")

# Add message to courtyard
world["science_courtyard"].message = "The sky is black and you cannot see the city."

# Add messages to corridors
world["nw_science_corridor"].message = "The lights are flickering here."
world["ne_science_corridor"].message = "The lights are flickering here."
world["sw_science_corridor"].message = "The lights are flickering here."
world["se_science_corridor"].message = "The lights are flickering here."
world["n_science_corridor"].message = "It is dim here."

# Add messages to classrooms
world["prime_room"].message = "The heaters are on here and they output a fine black dust."
world["caufield_room"].message = "YMCA is playing."
world["pirie_room"].message = "There is a sciency-smell here."

# Populate courtyard with items
world["science_courtyard"].addItems([shrubs, corpse])

# Populate corridors with items
world["nw_science_corridor"].addItems([crash, papers, corpse])
world["n_science_corridor"].addItems([corpse, goo])
world["ne_science_corridor"].addItem(papers)
world["sw_science_corridor"].addItems([corpse, pile])
world["se_science_corridor"].addItems([crash, corpse, papers, pile])

# Populate classrooms with items
world["prime_room"].addItems([pile, papers])
world["caufield_room"].addItems([Weapon("fly swat", 1, 1, 1), corpse])
world["pirie_room"].addItems([Weapon("meter ruler", 1, 1, 1), corpse, papers, pile])


# Link english and science blocks

# Create spaces
world["canteen_courtyard"] = dc(Space("courtyard"))
world["english-science_stairs"] = dc(Space("stairs"))
world["science_carpark"] = dc(Space("carpark"))

# Add messages to spaces
world["canteen_courtyard"].message = "The sky is black and you cannot see the city."
world["english-science_stairs"].message = "The sky is black and you cannot see the city."
world["science_carpark"].message = "The sky is black and you cannot see the city."

# Populate spaces with items
world["canteen_courtyard"].addItems([corpse])
world["science_carpark"].addItems([corpse])

# Link english block with courtyard
link("canteen_courtyard", "southwest", "nw_english_corridor")
link("canteen_courtyard", "southeast", "ne_english_corridor")

# Link courtyard with steps and steps with carpark
link("canteen_courtyard", "east", "english-science_stairs")
link("english-science_stairs", "east", "science_carpark")

# Link spaces with entrances
link("english-science_stairs", "northeast", "w_science_junction")
link("science_carpark", "northwest", "sw_science_corridor")
link("science_carpark", "northeast", "se_science_corridor")


# Math block

# Create spaces
world["math_corridor"] = dc(Space("corridor"))
world["henderson_room"] = dc(Space("classroom"))

# Add messages to spaces
world["math_corridor"].message = "There is a clean smell here."
world["henderson_room"].message = "There are many paper shapes hanging from the ceiling."

# Link corridor with classroom and carpark/stars with corridor
link("math_corridor", "west", "henderson_room")
link("english-science_stairs", "southeast", "math_corridor")
link("science_carpark", "southwest", "math_corridor")
