from item import Item, Weapon, Note
from space import Space
from world import world, encounters
from sub import title, combat
from character import Enemy

import os

name = title()

location = "robins_room"
history = []
bag = []
encounters_done = []
last = None

print ("You are in a {}.".format(world[location].name))
if world[location].message != None:
    print (world[location].message)
world[location].look(world)

def look(command, query, message):
    global last
    global world
    done = False

    if last != None and len(world[location].contains[last].contains) > 0:
        for i in world[location].contains[last].contains:
            if query in i.name:
                exec(command)
                done = True
                break

    if not done:
        for index, i in enumerate(world[location].contains):
            if query in i.name:
                exec(command)
                done = True
                last = index
                break

    if not done:
        print (message)

# Main loop
while True:
    try:

        iny = input("> ").replace(" the ", " ").replace(" in ", " ").split(" ")

        if iny[0] == "exit":
            raise IOError("exit")

        elif iny[0] == "go":

            done = False
            for i in world[location].routes:
                if i[1] == iny[1]:
                    history.append(location)
                    location = i[0]
                    done = True
                    last = None

                    if world[location].message != None:
                        print (world[location].message)

                    world[location].look(world)
                    break

            if not done:
                print ("There is nothing that way.")

            for i in encounters:
                if i.check(location, history, bag) and i not in encounters_done:
                    encounters_done.append(i)
                    print (i.message)
                    if i.combat != None:
                        combat(i.combat, bag)

        elif iny[0] == "look":

            if iny[1] == "bag":
                print ("In your bag there is:\n" + "\n".join(map(str, bag)))

            elif iny[1] == "around":
                print ("You are in a {}.".format(world[location].name))
                world[location].look(world)

            elif iny[1] == "at":
                look("i.look()", iny[2], "Look at what?")

        elif iny[0] == "pick":
            done = False
            fordel = []

            if last != None and len(world[location].contains[last].contains) > 0:
                for index, i in enumerate(world[location].contains[last].contains):
                    if iny[2] in i.name:
                        if i.movable:
                            fordel.append("world[location].contains[last].contains[" + str(index) + "]")
                            done = True
                            break
                        else:
                            print ("You can't pick up that item.")
                            done = True
                            break

            if not done:
                for index, i in enumerate(world[location].contains):
                    if iny[2] in i.name:
                        if i.movable:
                            fordel.append("world[location].contains[" + str(index) + "]")
                            done = True
                            break
                        else:
                            print ("You can't pick up that item.")
                            done = True
                            break

            if not done:
                print ("Pick up what?")

            for i in fordel:
                exec("bag.append(" + i + ")")
                exec("del " + i)
                print ("Added to bag.")

        elif iny[0] == "read":
            look("i.read()", iny[1], "Read what?")

        else:
            print ("Unknown commmand.")

    except KeyboardInterrupt:
        continue

    except IOError:
        name = title()

        location = "spronken_room"
        history = []
        bag = []
        encounters_done = []
        last = None

        print ("You are in a {}.".format(world[location].name))
        world[location].look(world)
