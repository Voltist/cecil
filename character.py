# General enemy class
class Enemy:

    # Init class that defines properties
    def __init__(self, name, health, weapon):
        self.name = name
        self.health = health
        self.weapon = weapon
