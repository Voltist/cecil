import os
from item import Item, Weapon
from character import Enemy
import random
import sys

# Title screen
def title():
    os.system("clear")
    print("WELCOME TO")
    os.system('figlet "' + "CECIL'S" + '" DOME')
    name = input("\nWhat is your name?: ")
    os.system("clear")

    print ("""
    You wake up in a wrecked classroom with a single, flickering bulb
    illuminating the space. Looking through a downlight above, you
    can see no sunlight, even though the clock on the wall indicates
    that the time is 1:46. There is a distant rumbling noise, and you
    can taste blood in your mouth.
    """)

    return name.capitalize()

# Death scenario
def death():
    os.system("figlet YOU DIED")
    input("Press enter to continue...")
    raise IOError("d")

# Combat scenario
def combat(enemy, bag):
    try:
        print ("\n    YOU HAVE ENTERED COMBAT WITH A " + enemy.name.upper() + "!\n")

        tmp = []
        for i in bag:
            if type(i) == Weapon:
                tmp.append(i)

        print ("Weapons in your inventory:\n*" + "\n* ".join(map(str, tmp)))

        while True:
            iny = input("Choose a weapon for combat: ")

            weapon = None
            for i in tmp:
                if iny in i.name:
                    weapon = i

            if weapon == None:
                print ("Please choose a valid weapon.")
            else:
                break

        print ("\nType 'swing', 'thrust', or 'hit' to use your weapon.\n")

        enemy_hp = 20
        player_hp = 20

        # Main combat loop
        while True:
            iny = input("> ")

            if iny == "swing":
                print ("You swing your weapon at the {}, causing {} damage!".format(enemy.name, str(weapon.sharpness)))
                enemy_hp -= weapon.sharpness

            elif iny == "thrust":
                print ("You thrust your weapon at the {}, causing {} damage!".format(enemy.name, str(weapon.pointiness)))
                enemy_hp -= weapon.pointiness

            elif iny == "hit":
                print ("You hit your weapon against the {}, causing {} damage!".format(enemy.name, str(weapon.bluntness)))
                enemy_hp -= weapon.bluntness

            else:
                print ("Unknown command. You stumble back.")

            # Enemy attacks
            choices = ["sharpness", "pointiness", "bluntness"]
            random.shuffle(choices)
            val = random.choice(choices)

            d = getattr(enemy.weapon, val)

            if enemy.weapon.magic:
                print ("The enemy cast a spell, causing {} damage!".format(d))

            else:
                if val == "sharpness":
                    print ("The enemy swings it's weapon at you, causing {} damage!".format(d))

                elif val == "pointiness":
                    print ("The enemy thrusts it's weapon at you, causing {} damage!".format(d))

                elif val == "bluntness":
                    print ("The enemy hits it's weapon against you, causing {} damage!".format(d))

            player_hp -= d

            if enemy_hp <= 0:
                print ("\n VICTORY! THE {} HAS PERISHED!\n".format(enemy.name.upper()))
                break

            elif player_hp <= 0:
                death()

    except KeyboardInterrupt:
        combat(enemy, bag)
